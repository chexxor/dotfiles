set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-sleuth'
Plugin 'tpope/vim-dispatch'
Plugin 'mkropat/vim-dwiw2015'
Plugin 'bling/vim-airline'
Plugin 'kien/ctrlp.vim'
set wildignore+=*/.git/*
let g:ctrlp_custom_ignore = '\v[\/]\.(git)$'

Plugin 'd11wtq/ctrlp_bdelete.vim'

Plugin 'jlanzarotta/bufexplorer'
" Keymappings
let mapleader = ","
noremap <Leader><t> :BufExplorer<return>
" map omni completion to Control + Space
inoremap <C-Space> <C-x><C-o>
inoremap <C-.> <C-x><C-o>
inoremap <C-@> <C-Space>

Plugin 'tpope/vim-surround'
Plugin 'neowit/vim-force.com'
"filetype plugin on
"syntax on
let g:apex_backup_folder=""
let g:apex_temp_folder=""
let g:apex_properties_folder=""
let g:apex_tooling_force_dot_com_path="~/Tools/tooling-force.com-0.3.6.2.jar"
let g:apex_syntax_case_sensitive=1
let g:apex_API_version="35.0"
let g:apex_conflict_check=0
let g:apex_server=1
let g:apex_server_timeoutSec=60*30
set completeopt=menu,preview,longest
function! s:setApexShortcuts()
	""""""""""""""""""""""""""""""""""""""""""
	" Search in files
	""""""""""""""""""""""""""""""""""""""""""
	" search exact word
	nmap <Leader>sc :noautocmd vimgrep /\<<C-R><C-W>\>/j ../**/*.cls ../**/*.trigger <CR>:cwin<CR>
	nmap <Leader>st :noautocmd vimgrep /\<<C-R><C-W>\>/j ../**/*.trigger <CR>:cwin<CR>
	nmap <Leader>sp :noautocmd vimgrep /\<<C-R><C-W>\>/j ../**/*.page <CR>:cwin<CR>
	nmap <Leader>so :noautocmd vimgrep /\<<C-R><C-W>\>/j ../**/*.object <CR>:cwin<CR>
	nmap <Leader>ss :noautocmd vimgrep /\<<C-R><C-W>\>/j ../**/*.scf <CR>:cwin<CR>
	nmap <Leader>sa :noautocmd vimgrep /\<<C-R><C-W>\>/j ../**/*.cls ../**/*.trigger ../**/*.page ../**/*.object ../**/*.scf <CR>:cwin<CR>
	" search - *contains* - partal match is allowed
	nmap <Leader>sC :noautocmd vimgrep /<C-R><C-W>/j ../**/*.cls ../**/*.trigger <CR>:cwin<CR>
	nmap <Leader>sT :noautocmd vimgrep /<C-R><C-W>/j ../**/*.trigger <CR>:cwin<CR>
	nmap <Leader>sP :noautocmd vimgrep /<C-R><C-W>/j ../**/*.page <CR>:cwin<CR>
	nmap <Leader>sS :noautocmd vimgrep /<C-R><C-W>/j ../**/*.object <CR>:cwin<CR>
	nmap <Leader>sS :noautocmd vimgrep /<C-R><C-W>/j ../**/*.scf <CR>:cwin<CR>
	nmap <leader>sA :noautocmd vimgrep /<C-R><C-W>/j ../**/*.cls ../**/*.trigger ../**/*.page ../**/*.object ../**/*/.scf <CR>:cwin<CR>
	" prepare search string, but do not run
	nmap <leader>sm :noautocmd vimgrep /\<<C-R><C-W>\>/j ../**/*.cls ../**/*.trigger ../**/*.page ../**/*.object ../**/*.scf \|cwin
	" search visual selection in Apex project
	function! ApexFindVisualSelection(searchType) range
		let l:apex_search_patterns = {'class': '../**/*.cls ../**/*.trigger', 
					\'trigger': '../**/*.trigger', 
					\'page': '../**/*.page', 
					\'all': '../**/*.cls ../**/*.trigger ../**/*.page ../**/*.scf'}
		let l:saved_reg = @"
		execute "normal! vgvy"
		let l:pattern = escape(@", '\\/.*$^~[]')
		let l:pattern = substitute(l:pattern, "\n$", "", "")
		let commandLine="noautocmd vimgrep " . '/'. l:pattern . '/j '
		let commandLine = commandLine . l:apex_search_patterns[a:searchType]
		"echo "commandLine=" . commandLine
		execute commandLine 
		execute 'cwin'
		let @/ = l:pattern
		let @" = l:saved_reg
	endfunction
	vmap <leader>sc :call ApexFindVisualSelection('class')<CR>
	vmap <leader>st :call ApexFindVisualSelection('trigger')<CR>
	vmap <leader>sp :call ApexFindVisualSelection('page')<CR>
	vmap <leader>sa :call ApexFindVisualSelection('all')<CR>
	""""""""""""""""""""""""""""""""""""""""""
	" CTags shortcuts
	""""""""""""""""""""""""""""""""""""""""""
	" shortcut to update ctags DB manually
	" note for XFCE: disable default workspace 11 switch (Ctrl-F11) shortcut
	" (settings-> Window Manager -> Keyboard),
	" otherwise C-F11 in vim does not work
	map <C-F11> <Esc>:ApexUpdateCtags<CR>
endfunction
" load shortcut mapping when one of apexcode file types is detected/loaded
autocmd FileType apexcode.java call s:setApexShortcuts()
autocmd FileType apexcode.html call s:setApexShortcuts()
autocmd FileType apexcode.javascript call s:setApexShortcuts()
autocmd FileType apexcode.html call s:setApexShortcuts()
"runtime ftdetect/vim-force.com.vim
"let g:apex_resources_unpacked_dir = '{resource-bundles,resources_unpacked}'
let g:apex_resources_unpacked_dir = "resource-bundles"
let g:apex_tooling_force_dot_com_java_params = "-DdeployOptions.allowMissingFiles=true"
"runtime ftdetect/bind-autocommands.vim


Plugin 'scrooloose/syntastic'
Plugin 'tpope/vim-obsession'
set sessionoptions=sesdir,winpos,resize,buffers

Plugin 'mileszs/ack.vim'
if executable('ag')
	let g:ackprg = 'ag --vimgrep --ignore-dir ".*" --ignore-dir "*.sublime-*"'
endif

Plugin 'raichoo/purescript-vim'
Plugin 'spf13/vim-colors'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on

" My Mappings
inoremap ;; <Esc>
noremap <Leader>as <Esc>:ApexSaveOne<CR>
noremap <Leader>ad <Esc>:ApexDeployOne<CR>
syntax on

colorscheme ir_black

highlight DiffAdd term=reverse cterm=bold ctermbg=green ctermfg=white 
highlight DiffChange term=reverse cterm=bold ctermbg=cyan ctermfg=black 
highlight DiffText term=reverse cterm=bold ctermbg=gray ctermfg=black 
highlight DiffDelete term=reverse cterm=bold ctermbg=red ctermfg=black 
highlight CursorLine cterm=none ctermbg=234 guibg=#3E3D32

au CursorHold,CursorHoldI * if getcmdwintype() == '' | checktime | endif
set autoread
set updatetime=2000
exec "set listchars=tab:\uBB\uBB,nbsp:~,trail:\uB7"
set list
set number

set suffixesadd+=.cls,.page,.component
set path+=~/workspace/sfdc/project

set isfname-=.

call ctrlp_bdelete#init()

