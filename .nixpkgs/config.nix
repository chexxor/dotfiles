let
  vim-version = "7.4.1941";
in
{
  allowBroken = true;
  packageOverrides = pkgs: rec {
    vim = pkgs.vim.overrideDerivation (oldAttrs: {
      name = "vim-${vim-version}";
      src = pkgs.fetchFromGitHub {
        owner = "vim";
        repo = "vim";
        rev = "v${vim-version}";
        sha256 = "0apq7sv1fbyfzqh4q0r2z19bn4gbjlb97wyl80kj7qsqmsy7m1lm";
      };
    });
    vimHugeX-mine = pkgs.vimHugeX.overrideDerivation (oldAttrs: {
      name = "vimHugeX-mine-${vim-version}";
      src = pkgs.fetchFromGitHub {
        owner = "vim";
        repo = "vim";
        rev = "v${vim-version}";
        sha256 = "0apq7sv1fbyfzqh4q0r2z19bn4gbjlb97wyl80kj7qsqmsy7m1lm";
      };  
    });
  };
}
